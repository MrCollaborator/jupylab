Jupyter Setup
============

Activate your Virtual Environment
------------------------------------------------
Activate your virtual environment. If you don't have one set upt yet, please do so first.

If the name of your venv directory is '.venv.':

**Windows:**
```.venv/Scripts/activate.ps1```

Install Requirements
--------------------
Use pip to install the necessary requirements

*Caution: You need admin/sudo rights to install jupyter*

```

pip install -r requirements.txt

Configure iPython Kernel for Jupyter
------------------------------------
Setup the iPython Kernel that we'll use in our Jupyter notebook.

ipython kernel install --user --name=.venv

Start the Notebook
------------------
Start the notebook by running the following command:

jupyter notebook
```